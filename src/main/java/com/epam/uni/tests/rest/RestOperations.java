package com.epam.uni.tests.rest;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.epam.uni.tests.utils.Constants;

public class RestOperations {

	private static final String REGISTRATION_URL_PART = "registration";
	private static final String DATA_GENERATION_URL_PART = "generateData";
	private static final String LOGIN_URL_PART = "login";
	private static final String CHANGE_DELAY_URL_PART = "changeDelay";
	private static final String LOAD_EVENTS_URL_PART = "loadEvents";

	public static void registerUser(User request) throws ClientProtocolException, IOException {
		HttpPost httpPost = new HttpPost(Constants.TRELLO_BASE_URL + REGISTRATION_URL_PART);
		StringEntity entity = new StringEntity(request.createJSONBody());
		httpPost.setEntity(entity);
		httpPost.setHeader("Accept", "application/json");
		httpPost.setHeader("Content-type", "application/json");

		CloseableHttpClient client = HttpClients.createDefault();
		client.execute(httpPost);
		client.close();
	}

	public static void generateData() throws ClientProtocolException, IOException {
		HttpGet httpGet = new HttpGet(Constants.TRELLO_BASE_URL + DATA_GENERATION_URL_PART);
		httpGet.setHeader("Accept", "application/json");
		httpGet.setHeader("Content-type", "application/json");

		CloseableHttpClient client = getLoggedInClient();
		client.execute(httpGet);
		client.close();
	}

	public static void changeDelay(int delay) throws ClientProtocolException, IOException {
		CloseableHttpClient client = getLoggedInClient();
	
		client.execute(createHttpPost(Constants.TRELLO_BASE_URL + CHANGE_DELAY_URL_PART,
				"delayAmount=" + String.valueOf(delay)));
		client.close();
	}

	public static int getNumberOfEvents() throws ClientProtocolException, IOException {
		CloseableHttpClient client = getLoggedInClient();
		HttpGet httpGet = new HttpGet(Constants.TRELLO_BASE_URL + LOAD_EVENTS_URL_PART);

		CloseableHttpResponse response = client.execute(httpGet);
		HttpEntity entity = response.getEntity();

		String content = EntityUtils.toString(entity);
		client.close();
		return content.split("id").length - 1;
	}

	private static CloseableHttpClient getLoggedInClient() throws ClientProtocolException, UnsupportedEncodingException, IOException {
		CloseableHttpClient client = HttpClients.createDefault();
		client.execute(
				createHttpPost(Constants.TRELLO_BASE_URL + LOGIN_URL_PART, "password=admin&username=admin"));
		return client;
	}
	
	private static HttpPost createHttpPost(String url, String body) throws UnsupportedEncodingException {
		HttpPost httpPost = new HttpPost(url);
		httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
		StringEntity entity = new StringEntity(body);
		httpPost.setEntity(entity);
		return httpPost;
	}
}
