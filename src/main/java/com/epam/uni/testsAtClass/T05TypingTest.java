package com.epam.uni.testsAtClass;

import static org.hamcrest.Matchers.emptyString;
import static org.hamcrest.Matchers.is;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class T05TypingTest extends TestBase {
	
	private final String SPORTS_BET_URL = "http://localhost:8080/sportsbetting-web/login";
	
	@Test
	public void sendAndClearUsernameField() {
		driver.get(SPORTS_BET_URL);
        WebElement username = driver.findElement(By.name("username"));
        // Type admin word to username
		username.sendKeys("admin");
		Assert.assertEquals("admin", username.getAttribute("value"));
		
        // Clear username input field
		username.clear();

		Assert.assertThat(username.getAttribute("value"), is(emptyString()));
	}
}
