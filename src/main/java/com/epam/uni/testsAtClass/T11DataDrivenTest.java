package com.epam.uni.testsAtClass;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.FileParameters;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class T11DataDrivenTest extends TestBase {

	/***
	 * 
	 * Description: test case to demonstrate DDT parameterized test case
	 * with @FileParameters annotation
	 * 
	 * @param user
	 * @param password
	 * @param expectedTitle
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	@Test
	@FileParameters("src/main/resources/tdd/test.csv")
	public void testLoginWithFileParameters(String user, String password, String expectedTitle)
			throws ClientProtocolException, IOException {
		login(user, password);
		assertThat(driver.getTitle(), is(expectedTitle));
	}

	/***
	 * 
	 * Description: test case to demonstrate DDT parameterized test case with data provider method
	 * with @FileParameters annotation
	 * 
	 * @param user
	 * @param password
	 * @param expectedTitle
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	@Test
	@Parameters(method = "getTestData")
	public void testLoginWithParameterMethod(TestData data) throws ClientProtocolException, IOException {
		login(data.user, data.password);
		assertThat(driver.getTitle(), is(data.expectedTitle));
	}

	public Object[] getTestData() {
		Object[] testDataArray = new Object[3];
		testDataArray[0] = new TestData("admin", "admin", "SportsBetting Home");
		testDataArray[1] = new TestData("admin", "invalid_password", "SportsBet Login");
		testDataArray[2] = new TestData("invalid_user", "invalid_password", "SportsBet Login");
		return testDataArray;
	}

	private class TestData {
		private String user;
		private String password;
		private String expectedTitle;

		public TestData(String user, String password, String expectedTitle) {
			this.user = user;
			this.password = password;
			this.expectedTitle = expectedTitle;
		}
	}
}
