package com.epam.uni.testsAtClass;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.epam.uni.builders.LoginModelBuilder;
import com.epam.uni.entities.LoginModel;
import com.epam.uni.pagemodels.pages.LoginPage;

public class Homework extends TestBase {
	
	private void takeScreenshot(String screenshotPath) {
		File screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(screenshot, new File(screenshotPath));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void my_test() {
		WebDriverWait wait = new WebDriverWait(driver, 60);  // timeout after 60 seconds
		
		// ! Open the Trello login page
		LoginModel loginModel = LoginModelBuilder.simpleBuilder().build();
		LoginPage.navigateTo(driver);
		
		takeScreenshot("./screenshot_1_login_page.png");
		
		// ! Login with the given credentials
		LoginPage loginPage = LoginPage.create(driver);
		loginPage.fillLoginForm(loginModel);
		
		takeScreenshot("./screenshot_2_login_filled.png");
		
		// wait for an element of logged in page to be seen: Login was successful, and user board page loaded.
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("header-boards-button")));
		
		takeScreenshot("./screenshot_3_logged_in.png");
		
		// ! Open own table: Balazs_Szarvas
		driver.findElement(By.cssSelector("div[title='Balazs_Szarvas']")).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("textarea[aria-label='Todo']")));
		
		takeScreenshot("./screenshot_4_my_board.png");
		
		// ! In the �Todo� list of your own table create a new card as per the followings: Monogram + the actual timestamp
		driver.findElements(By.cssSelector("span[class='js-add-a-card']")).get(0).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("textarea[placeholder='Enter a title for this card�']")));

		Calendar cal = Calendar.getInstance();
		String cardText = "BSZ " + cal.getTime();
		driver.findElement(By.cssSelector("textarea[placeholder='Enter a title for this card�']")).sendKeys(cardText);
		driver.findElement(By.cssSelector("input[value='Add Card']")).click();
		
		takeScreenshot("./screenshot_5_card_created.png");
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(cardText))); // wait for card to be present in the list
		
		// ! Move this created card into the �Done� list
		driver.findElement(By.linkText(cardText)).click();  // click on card to bring up menu, where we can click Move
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Move")));
		driver.findElement(By.linkText("Move")).click();

		Select dropdown = new Select(driver.findElement(By.className("js-select-list")));  // Select Done list for moving card
		dropdown.selectByVisibleText("Done");
		driver.findElement(By.cssSelector("input[value='Move']")).click();
		driver.findElement(By.cssSelector("a[class='icon-lg icon-close dialog-close-button js-close-window']")).click();
		
		takeScreenshot("./screenshot_6_card_moved.png");
		
		// ! Assertion: the card with the given name is in the �Done� list
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(cardText)));
		driver.findElement(By.linkText(cardText)).click();
		String cardListName = driver.findElement(By.cssSelector("a[class='js-open-move-from-header']")).getText();
		Assert.assertTrue(cardListName.equals("Done"));
	}
	
}
