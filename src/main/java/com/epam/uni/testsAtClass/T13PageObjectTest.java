package com.epam.uni.testsAtClass;

import static org.hamcrest.Matchers.emptyString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

import org.junit.Assert;
import org.junit.Test;

import com.epam.uni.builders.LoginModelBuilder;
import com.epam.uni.entities.LoginModel;
import com.epam.uni.pagemodels.pages.AccountDetailsPage;
import com.epam.uni.pagemodels.pages.LoginPage;

public class T13PageObjectTest extends TestBase {
	
	@Test
    public void loginShouldWorkTest()
    {
		// Given
		LoginModel loginModel = LoginModelBuilder.simpleBuilder().build();
		
		// When
	    // TASK 1.1: implement a static navigate function to sportsbetting page
		LoginPage.navigateTo(driver);
		// TASK 1.2: implement create method in LoginPage
		LoginPage loginPage = LoginPage.create(driver);
		// TASK 2.1: implement fillLoginForm method use the loginModel
		loginPage.fillLoginForm(loginModel);
		AccountDetailsPage account = AccountDetailsPage.create(driver);
		
		// Then
		Assert.assertTrue(account.isPageDisplay());
    }

	@Test
	public void accountDetailsAllFieldsHaveValue()
    {
		// Given
		LoginModel loginModel = LoginModelBuilder.simpleBuilder().build();
		
		// When
		LoginPage.navigateTo(driver);
		LoginPage loginPage = LoginPage.create(driver);
		loginPage.fillLoginForm(loginModel);
		AccountDetailsPage account = AccountDetailsPage.create(driver);
		
		// Then
		Assert.assertThat(account.nameField.getAttribute("value"), is(not(emptyString())));
		Assert.assertThat(account.dateOfBirth.getAttribute("value"), is(not(emptyString())));
		Assert.assertThat(account.accountNumber.getAttribute("value"), is(not(emptyString())));
		Assert.assertThat(account.balance.getAttribute("value"), is(not(emptyString())));
		Assert.assertThat(account.currency.getAttribute("value"), is(not(emptyString())));
    }
}
