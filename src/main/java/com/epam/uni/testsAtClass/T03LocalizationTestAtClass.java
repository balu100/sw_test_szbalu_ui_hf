package com.epam.uni.testsAtClass;
import static org.hamcrest.CoreMatchers.containsString;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;

public class T03LocalizationTestAtClass extends TestBase {

	@Test
	public void findSportsBetTitle() {
        driver.get("http://localhost:8080/sportsbetting-web/login");

        // Use css selector to localize the title Üdvözlünk a SportsBeten!
        By cssSelector = null;
        String title = driver.findElement(cssSelector).getText();
        Assert.assertThat("Üdvözlünk a SportsBeten!", containsString(title));
	}

	@Test
	public void findHomePageLinks() {
        driver.get("http://localhost:8080/sportsbetting-web/login");

        // use cssSelector to login link localization
        By loginLinkLocator = null;
        String loginLink = driver.findElement(loginLinkLocator).getText();
        Assert.assertEquals("Bejelentkezés", loginLink);
        
        // use XPath to register link localization
        By registerLinkLocator = null;
        String registerLink = driver.findElement(registerLinkLocator).getText();
        Assert.assertEquals("Regisztrálás", registerLink);
	}
}
