package com.epam.uni.testsAtClass;

import static org.hamcrest.Matchers.containsString;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.http.client.ClientProtocolException;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.epam.uni.tests.rest.RestOperations;

public class T07SynchronizationTest extends TestBase {

	/**
	 * Description: test case to demonstrate the working of the WebDriverWait class
	 * 
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	@Test
	public void testWaitForHeaderToChange() throws ClientProtocolException, IOException {
		RestOperations.changeDelay(1000);

		loginWithAdminUser();

		// create a WebDriverWait which waits until the page URL contains "SportsBetting Home"
		WebDriverWait wait = new WebDriverWait(driver, 2);
		wait.until(ExpectedConditions.titleIs("SportsBetting Home"));
	}

	/**
	 * Description: test case to demonstrate the working of the WebDriverWait class
	 * 
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	@Test
	public void testWaitForUrlToChange() throws ClientProtocolException, IOException {
		RestOperations.changeDelay(2000);

		loginWithAdminUser();

		// create a WebDriverWait which waits until the page URL contains "/home"
		WebDriverWait wait = new WebDriverWait(driver, 2);
		wait.until(ExpectedConditions.urlContains("/home"));
		Assert.assertThat(driver.getCurrentUrl(), containsString("/sportsbetting-web/home"));

		driver.findElement(By.xpath("//a[@href='events']")).click();

		// create a WebDriverWait which waits until the page URL contains "/events"
		wait.until(ExpectedConditions.urlContains("/events"));
		Assert.assertThat(driver.getCurrentUrl(), containsString("/sportsbetting-web/events"));

	}

	/**
	 * Description: test case to demonstrate the working of the pageLoadTimeout
	 * 
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	@Test
	public void waitForPageLoad() throws ClientProtocolException, IOException {
		RestOperations.changeDelay(1000);
		// set page load time out
		// 1. to 5000 milliseconds result PASSED
		// 2. to 50 milliseconds result FAILED
		driver.manage().timeouts().pageLoadTimeout(5000, TimeUnit.MILLISECONDS);
		driver.get(LOGIN_PAGE_URL);
		Assert.assertThat(driver.getTitle(), containsString("SportsBet Login"));
	}
}
