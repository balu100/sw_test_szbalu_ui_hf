package com.epam.uni.testsAtClass;

import static org.hamcrest.Matchers.emptyString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class T10CookiesTest extends TestBase {

	private static final String COOKIE_UNDER_TEST = "JSESSIONID";

	/**
	 * Description: test case to demonstrate selenium's cookie handling capabilities #1
	 */
	@Test
	public void testCookieOperations() {
		loginWithAdminUser();

		// find JSESSIONID cookie
		Cookie sessionIdBefore = driver.manage().getCookieNamed(COOKIE_UNDER_TEST);
		Assert.assertThat(sessionIdBefore.getValue(), not(emptyString()));

		driver.findElement(By.id("logout")).click();

		loginWithAdminUser();

		// find JSESSIONID cookie again
		Cookie sessionIdAfter = driver.manage().getCookieNamed(COOKIE_UNDER_TEST);
		Assert.assertThat(sessionIdAfter.getValue(), is(not(equalTo(sessionIdBefore))));
	}

	/**
	 * Description: test case to demonstrate selenium's cookie handling capabilities #2
	 */
	@Test
	public void testCookieDeletion(){
	    final String cookieToTest = "visitorid";
	    final String booklineURL = "http://bookline.hu";
		
		// delete all cookies
		driver.manage().deleteAllCookies();
		
		driver.navigate().to(booklineURL);
		acceptCookies();
		// get VISITOR_ID_COOKIE into cookieUnderTest variable
		Cookie cookieUnderTest = driver.manage().getCookieNamed(cookieToTest);
		// verify that the test cookie exists
		Assert.assertNotEquals("", cookieUnderTest.getValue());
		
		// delete cookieToTest cookie
		driver.manage().deleteCookieNamed(cookieToTest);
		// get VISITOR_ID_COOKIE into deletedCookie variable
		Cookie deletedCookie = driver.manage().getCookieNamed(cookieToTest);
		Assert.assertNull(deletedCookie);
		
		searchForBook("Java ee");
		// get cookie into recreatedCookie variable 
		Cookie recreatedCookie = driver.manage().getCookieNamed(cookieToTest);
		Assert.assertNotEquals(cookieUnderTest.getValue(), recreatedCookie.getValue());
	}
	
	private void acceptCookies() {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement acceptCookie = wait.until(ExpectedConditions.elementToBeClickable(By.id("CybotCookiebotDialogBodyButtonAccept")));
        if (acceptCookie != null) {
    		driver.findElement(By.id("CybotCookiebotDialogBodyButtonAccept")).click();
        }
	}
	
	private void searchForBook(String bookName) {
		WebElement searchField = driver.findElement(By.id("search_form_field"));
		searchField.sendKeys(bookName);
		
		WebElement searchBtn = driver.findElement(By.cssSelector("button[class='btn btn-green btn-sm search inverse-icon']"));
		searchBtn.submit();
	}
}
