package com.epam.uni.testsAtClass;

import static org.hamcrest.CoreMatchers.anything;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.endsWith;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;

public class T01AssertionTest {

	@Test
	public void assertionsBasicExample() {
		assertTrue(true);
		assertFalse(false);
		
		assertNull(null);
		assertNotNull(1);
		
		assertTrue("".isEmpty());
		assertTrue((new ArrayList<Object>()).isEmpty());

		assertFalse("not empty".isEmpty());
		assertFalse(Arrays.asList(1).isEmpty());
	}
	
	@Test
	public void assertionAdvancedExample() {
		Integer[] array1 = {1,2};
		Integer[] array2 = {1,2};
		Integer[] array3 = array1;
		Integer[] array4 = {1,3};
		
		// change second parameters of the assertions
		assertArrayEquals(array1, array2);
		
		assertFalse(array1.equals(array2));
		assertTrue(array1.equals(array3));

		assertThat(array1, equalTo(array2));
		assertThat(array1, not(equalTo(array4)));
		
		assertSame(array1, array3);
		assertNotSame(array1, array2);
		
		// Contains would not work with primitive!!
		assertTrue(Arrays.asList(array1).contains(1));
	}
	
	@Test
	public void assertionsStringExample() {
		assertThat("Hope is a waking dream", containsString("waking"));
		assertThat("Hope is a waking dream", startsWith("Hope"));
		assertThat("Hope is a waking dream", endsWith("dream"));
		assertThat("Hope is a waking dream", is(anything("is\\s.*\\sdream")));
	}
}
