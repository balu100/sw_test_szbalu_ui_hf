package com.epam.uni.testsAtClass;
import static org.hamcrest.CoreMatchers.containsString;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;

public class T03LocalizationTest extends TestBase {

	@Test
	public void findSportsBetTitle() {
        driver.get("http://localhost:8080/sportsbetting-web/login");

        // Use css selector to localize the title Üdvözlünk a SportsBeten!
        By cssSelector = By.cssSelector("header>span");
        String title = driver.findElement(cssSelector).getText();
        Assert.assertThat("Üdvözlünk a SportsBeten!", containsString(title));
	}

	@Test
	public void findHomePageLinks() {
        driver.get("http://localhost:8080/sportsbetting-web/login");

        // use cssSelector to login link localization
        By loginLinkLocator = By.cssSelector("a[href='login']");
        String loginLink = driver.findElement(loginLinkLocator).getText();
        Assert.assertEquals("Bejelentkezés", loginLink);

        // use XPath to register link localization
        By registerLinkLocator = By.xpath("//a[@href='register']");
        String registerLink = driver.findElement(registerLinkLocator).getText();
        Assert.assertEquals("Regisztrálás", registerLink);
	}
}
