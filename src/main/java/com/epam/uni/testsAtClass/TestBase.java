package com.epam.uni.testsAtClass;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.http.client.ClientProtocolException;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import com.epam.uni.tests.rest.RestOperations;
import com.epam.uni.tests.rest.User;
import com.epam.uni.tests.utils.Constants;

public class TestBase {
	private static final int NUMBER_OF_EVENTS_TO_GENERATE = 20;
	protected static final String LOGIN_PAGE_URL = Constants.TRELLO_BASE_URL + "login";

	protected WebDriver driver = null;

	@Before
	public void setUp() throws ClientProtocolException, IOException {
		if (RestOperations.getNumberOfEvents() < NUMBER_OF_EVENTS_TO_GENERATE) {
			generateEvents(NUMBER_OF_EVENTS_TO_GENERATE);
		}

		RestOperations.changeDelay(0);
		System.setProperty("webdriver.chrome.driver", "src/main/resources/drivers/chromedriver.exe");
		driver = getChromeDriver("hu");
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
	}

	@After
	public void tearDown() {
		driver.quit();
	}

	public WebDriver getChromeDriver(String lang) {
		ChromeOptions options = new ChromeOptions();
		options.addArguments("disable-infobars");
		options.addArguments("--lang=" + lang);
		return new ChromeDriver(options);
	}

	protected void loginWithAdminUser() {
		login("admin", "admin");
	}

	protected void generateEvents(int numberOfEvents) throws ClientProtocolException, IOException {
		for (int i = 0; i < numberOfEvents; i++) {
			RestOperations.generateData();
		}
	}

	protected void login(String user, String password) {
		driver.get(LOGIN_PAGE_URL);

		WebElement usernameInput = driver.findElement(By.name("username"));
		WebElement passwordInput = driver.findElement(By.name("password"));
		WebElement loginButton = driver.findElement(By.id("login-button"));

		usernameInput.sendKeys(user);
		passwordInput.sendKeys(password);
		loginButton.click();
	}

	//method to a specific predefined user , currently not used probably it won't be necessary and can be deleted 
	private void registerTestUser() throws ClientProtocolException, IOException {
		User user = new User.Builder().withAccountNumber("123456789").withBalance(String.valueOf("10000"))
				.withCurrency("HUF").withName("Test Name").withPassword("test").withUserName("test")
				.withDateOfBirth("1991-11-11").build();
		RestOperations.registerUser(user);
	}

}
