package com.epam.uni.testsAtClass;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

public class T06ListboxCheckBoxTestAtClass extends TestBase {
	
	private final String SPORTS_BET_URL = "http://localhost:8080/sportsbetting-web/login";
	
	@Test
	public void changeLanguageAfterLogin() {
		driver.get(SPORTS_BET_URL);
        driver.findElement(By.name("username")).sendKeys("admin");
        driver.findElement(By.name("password")).sendKeys("admin");
        driver.findElement(By.id("login-button")).click();

        String accountDetailsHu = driver.findElement(By.cssSelector("#player-info>.box-header")).getText();

		// select the en value from the language selector list box
		
        String accountDetailsEn = driver.findElement(By.cssSelector("#player-info>.box-header")).getText();
		Assert.assertNotEquals(accountDetailsHu, accountDetailsEn);
	}
}
