package com.epam.uni.testsAtClass;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

public class T09ScreenshotTest extends TestBase {

	@Test
	public void screenshotTest() throws IOException {
		loginWithAdminUser();
		// Get screenshot
		File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		String screenshotPath = getScreenshotName();
		FileUtils.copyFile(screenshot, new File(screenshotPath));
		Assert.assertTrue(isSavedScreenshotFile(screenshotPath));
	}

	private boolean isSavedScreenshotFile(String screenshotPath) {
		File dir = new File("target/screenshots");
		File[] matchingFiles = dir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return screenshotPath.contains(name);
			}
		});
		return matchingFiles != null && matchingFiles.length != 0;
	}

	private String getScreenshotName() {
		StackTraceElement stack = Thread.currentThread().getStackTrace()[2];
		String[] classArray = stack.getClassName().split("\\.");
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy_hh-mm-ss");
		Date curDate = new Date();
		String strDate = sdf.format(curDate);
		return "target/screenshots/" + classArray[(classArray.length - 1)] + "_" + stack.getMethodName() + "_" + strDate
				+ "_error.jpg";
	}
}
