package com.epam.uni.testsAtClass;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class T04ClickTest extends TestBase {
	
	private String SPORTS_BET_URL = "http://localhost:8080/sportsbetting-web/login";
	
	@Test
	public void testButtonClick() {
        driver.get(SPORTS_BET_URL);

        String originalTitle = driver.findElement(By.cssSelector("header>span")).getText();
        // click on English in the top right corner
        driver.findElement(By.cssSelector("div[class='langChanger']>a[href*='lang=en']")).click();
        String englishTitle = driver.findElement(By.cssSelector("header>span")).getText();
        
        Assert.assertNotEquals(originalTitle, englishTitle);
        

        // click on Registration link
        driver.findElement(By.cssSelector("a[href='register']")).click();
        Assert.assertEquals("Player Registration", driver.getTitle());
	}
	
	@Test
	public void testModifiedClick() {
		driver.get(SPORTS_BET_URL);

		//Use Actions class to create a shift click on Register link
        //You have to see two browser windows after a successful run
		new Actions(driver).keyDown(Keys.SHIFT).click(driver.findElement(By.cssSelector("a[href='register']"))).keyUp(Keys.SHIFT).build().perform();

		Assert.assertEquals(2, driver.getWindowHandles().size());
	}
	
	@Test
	public void testDragAndDrop() {
		driver.get("https://jqueryui.com/resources/demos/droppable/default.html");
        //Locate the draggable element
        WebElement draggable = driver.findElement(By.id("draggable"));
        //Locate the droppable element
        WebElement droppable = driver.findElement(By.id("droppable"));
        //Add the drag and drop action here
        new Actions(driver).dragAndDrop(draggable, droppable).build().perform();
        Assert.assertTrue(driver.findElement(By.cssSelector("#droppable > p:nth-child(1)")).getText().equals("Dropped!"));
	}
}
