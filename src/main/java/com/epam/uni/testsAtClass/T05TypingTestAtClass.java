package com.epam.uni.testsAtClass;

import static org.hamcrest.Matchers.emptyString;
import static org.hamcrest.Matchers.is;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class T05TypingTestAtClass extends TestBase {
	
	private final String SPORTS_BET_URL = "http://localhost:8080/sportsbetting-web/login";
	
	@Test
	public void sendAndClearUsernameField() {
		driver.get(SPORTS_BET_URL);
        WebElement username = driver.findElement(By.name("username"));
        // Type admin word to username

        Assert.assertEquals("admin", username.getAttribute("value"));
		
        // Clear username input field
		Assert.assertThat(username.getAttribute("value"), is(emptyString()));
	}
}
