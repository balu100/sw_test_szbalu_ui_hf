package com.epam.uni.testsAtClass;

import java.net.MalformedURLException;
import java.net.URL;

import org.junit.Assert;
import org.junit.Test;

public class T02NavigationTestAtClass extends TestBase {
	
	private final String GOOGLE_URL = "http://www.google.com";
	private final String EPAM_URL = "http://www.epam.com";
	private final String SPORTS_BETTING = "http://localhost:8080/sportsbetting-web/login/";
	
	private String assertTitleChange(String oldTitle) {
		String newTitle = driver.getTitle();
		Assert.assertNotEquals(oldTitle, newTitle);
		return newTitle;
	}
	
	@Test
	public void testNavigation() throws MalformedURLException {
		final URL SPORTS_BETTING_URL_FORMAT = new URL(SPORTS_BETTING);
		String title = "";
		
		//Navigate to http://www.google.com -> GOOGLE_URL
		title = assertTitleChange(title);

		//Navigate to http://www.epam.com
		title = assertTitleChange(title);
        
		//Navigate to http://localhost:8080/sportsbetting-web/login with url format -> SPORTS_BETTING_URL_FORMAT
		title = assertTitleChange(title);
		
		//Navigate back
		title = assertTitleChange(title);
		
		//Navigate forward
		driver.navigate().forward();
		title = assertTitleChange(title);
		
		//Refresh browser window
		Assert.assertEquals(title, driver.getTitle());

		//Create an assert and check current URL
		Assert.assertEquals(SPORTS_BETTING, driver.getCurrentUrl());
    }
}
