package com.epam.uni.testsAtClass;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.epam.uni.testsAtClass.TestBase;

public class T08WindowTestAtClass extends TestBase {

	@Test
	public void responsiveWindow() {
		driver.navigate().to("https://www.expedia.com/");

		// Maximize browser window

		Assert.assertTrue(driver.findElement(By.id("header-account-menu")).isDisplayed());
		Assert.assertFalse(driver.findElement(By.id("header-mobile-toggle")).isEnabled());

		// Set browser window size to 600x600

		Assert.assertTrue(driver.findElement(By.id("header-mobile-toggle")).isDisplayed());
		Assert.assertFalse(driver.findElements(By.id("header-account-menu")).isEmpty());
	}

	@Test
	public void multipleWindow() {
		driver.get("https://www.amazon.com/gp/gw/ajax/s.html");

		new Actions(driver).keyDown(Keys.SHIFT).click(driver.findElement(By.linkText("Today's Deals"))).keyUp(Keys.SHIFT).build().perform();

		switchToLastOpenedWindow();
		WebElement todaysDeals = driver.findElement(By.className("gbh1-bold"));
		Assert.assertTrue(todaysDeals.isDisplayed());
		Assert.assertEquals("Today's Deals", todaysDeals.getText());

		driver.close();

		switchToLastOpenedWindow();
		new Actions(driver).keyDown(Keys.SHIFT).click(driver.findElement(By.id("nav-your-amazon"))).keyUp(Keys.SHIFT).build().perform();
		switchToLastOpenedWindow();
		WebElement signIn = driver.findElement(By.cssSelector("[name='signIn'] .a-spacing-small"));
		Assert.assertTrue(signIn.isDisplayed());
		Assert.assertEquals("Sign in", signIn.getText());

		driver.close();
	}

	/**
	 * switch to the last opened window
	 */
	private void switchToLastOpenedWindow() {
		// Switch to last opened window
	}
}
