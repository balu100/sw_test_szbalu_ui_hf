package com.epam.uni.pagemodels.pages;

import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

public class BasePage {

	protected WebDriver driver;
	protected By layerLocator = By.tagName("body");
	
	public BasePage(WebDriver driver) {
		this.driver = driver;
	}
	
	public static void navigateTo(WebDriver driver) {
		driver.get("https://trello.com/login");
	}

	public boolean isPageDisplay() {
	   Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
		       .withTimeout(30, TimeUnit.SECONDS)
		       .pollingEvery(1, TimeUnit.SECONDS)
		       .ignoring(NoSuchElementException.class);

	   WebElement foo = wait.until(new Function<WebDriver, WebElement>() {
	     public WebElement apply(WebDriver driver) {
	       return driver.findElement(layerLocator);
	     }
	   });
	   
	   if (foo != null)
		   return true;
	   else 
		   return false;
	}
}
