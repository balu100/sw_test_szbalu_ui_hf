package com.epam.uni.pagemodels.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.epam.uni.entities.LoginModel;

public class LoginPage extends BasePage {
	
	public LoginPage(WebDriver driver) {
		super(driver);
	}
	
	public static LoginPage create(WebDriver driver) {
		return PageFactory.initElements(driver, LoginPage.class);
	}

	@FindBy(how = How.NAME, using = "user")
	public WebElement usernameField;

	@FindBy(how = How.NAME, using = "password")
	private WebElement passwordField;
	
	@FindBy(how = How.ID, using = "login")
	private WebElement loginButton;
	
	public void fillLoginForm(LoginModel model)
	{
		setUsername(model.getUsername());
		setPassword(model.getPassword());
		clickLoginButton();
	}
	
	public void setUsername(String value)
	{
		if (value != null)
		{
			usernameField.sendKeys(value);
		}
	}
	
	public void setPassword(String value)
	{
		if (value != null)
		{
			passwordField.sendKeys(value);
		}
	}
	
	public void clickLoginButton()
	{
		loginButton.click();
	}
}
