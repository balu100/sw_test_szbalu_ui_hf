package com.epam.uni.pagemodels.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class AccountDetailsPage extends BasePage {
	
	@FindBy(how = How.CSS, using = "[ng-model='player.name']")
	public WebElement nameField;
	
	@FindBy(css = "[ng-model='player.dateOfBirth']")
	public WebElement dateOfBirth;
	
	@FindBy(css = "[ng-model='player.accountNumber']")
	public WebElement accountNumber;
	
	@FindBy(css = "[ng-model='player.balance']")
	public WebElement balance;
	
	@FindBy(css = "[ng-model='player.currency']")
	public WebElement currency;
	
	public AccountDetailsPage(WebDriver driver) {
		super(driver);
		layerLocator = By.id("player-info");
	}

	public static AccountDetailsPage create(WebDriver driver) {
		return PageFactory.initElements(driver, AccountDetailsPage.class);
	}
}
